//
// To build manually:
//
// OS X:
//    c++ -O3 -Wall -shared -std=c++11 -undefined dynamic_lookup $(python3 -m pybind11 --includes) -I./src src/ctvx.cpp -o ctvx$(python3-config --extension-suffix)
//
// Linux with pipenv:
//    pipenv run c++ -O3 -Wall -shared -std=c++11 -fPIC $(pipenv run python3 -m pybind11 --includes) src/ctvx.cpp -o build/lib/ctvx$(pipenv run python3-config --extension-suffix)
//

#include <cmath>
#include "ctvx.hpp"


std::shared_ptr<Tvf> ramp(
    double frm,
    double to,
    double start,
    double duration
) {
    Tvf *tvf_time = new TvfTime();
    Tvf *tvf = new TvfRamp(
        std::shared_ptr<Tvf>(tvf_time),
        std::shared_ptr<Tvf>(new TvfConst(frm)),
        std::shared_ptr<Tvf>(new TvfConst(to)),
        start, duration
    );
    return std::shared_ptr<Tvf>(tvf);
}

std::shared_ptr<Tvf> ramp(
    std::shared_ptr<Tvf> frm,
    double to,
    double start,
    double duration
) {
    Tvf *tvf_time = new TvfTime();
    Tvf *tvf = new TvfRamp(
        std::shared_ptr<Tvf>(tvf_time),
        frm,
        std::shared_ptr<Tvf>(new TvfConst(to)),
        start, duration
    );
    return std::shared_ptr<Tvf>(tvf);
}

std::shared_ptr<Tvf> ramp(
    double frm,
    std::shared_ptr<Tvf> to,
    double start,
    double duration
) {
    Tvf *tvf_time = new TvfTime();
    Tvf *tvf = new TvfRamp(
        std::shared_ptr<Tvf>(tvf_time),
        std::shared_ptr<Tvf>(new TvfConst(frm)),
        to,
        start, duration
    );
    return std::shared_ptr<Tvf>(tvf);
}

std::shared_ptr<Tvf> ramp(
    std::shared_ptr<Tvf> frm,
    std::shared_ptr<Tvf> to,
    double start,
    double duration
) {
    Tvf *tvf_time = new TvfTime();
    Tvf *tvf = new TvfRamp(
        std::shared_ptr<Tvf>(tvf_time),
        frm,
        to,
        start, duration
    );
    return std::shared_ptr<Tvf>(tvf);
}


std::shared_ptr<Tvf> once(std::shared_ptr<Tvf> x) {
    return std::shared_ptr<Tvf>(new TvfOnce(x));
}


std::shared_ptr<Tvf> cut(
    std::shared_ptr<Tvf> before,
    double t,
    std::shared_ptr<Tvf> after
)
{
    if(TvfCut *cut_before = dynamic_cast<TvfCut *>(before.get())) {
        std::vector<std::shared_ptr<Tvf>> values =
            std::vector<std::shared_ptr<Tvf>>(cut_before->values());
        std::vector<double> times = std::vector<double>(cut_before->cut_times());

        values.push_back(after);
        times.push_back(t);

        return std::shared_ptr<Tvf>(new TvfCut(values, times));
    } else {
        std::vector<std::shared_ptr<Tvf>> values{ before, after };
        std::vector<double> times{ t };

        return std::shared_ptr<Tvf>(new TvfCut(values, times));
    }
}


std::shared_ptr<Tvf> cut(
    double before,
    double t,
    std::shared_ptr<Tvf> after
)
{
    return cut(std::shared_ptr<Tvf>(new TvfConst(before)), t, after);
}


std::shared_ptr<Tvf> cut(
    std::shared_ptr<Tvf> before,
    double t,
    double after
)
{
    return cut(before, t, std::shared_ptr<Tvf>(new TvfConst(after)));
}


std::shared_ptr<Tvf> cut(
    double before,
    double t,
    double after
)
{
    return cut(
        std::shared_ptr<Tvf>(new TvfConst(before)),
        t,
        std::shared_ptr<Tvf>(new TvfConst(after))
    );
}


std::shared_ptr<Tvf> sample(std::shared_ptr<Tvf> x, std::vector<double> &sample_times) {
    return std::shared_ptr<Tvf>(new TvfSample(x, sample_times));
}


#define IF_THEN_ELSE(V, E, AT, AF, VC)                                                              \
std::shared_ptr<V> if_then_else(                                                                    \
    std::shared_ptr<Tvb> cond,                                                                      \
    AT val_if_true,                                                                                 \
    AF val_if_false                                                                                 \
)                                                                                                   \
{                                                                                                   \
    return std::shared_ptr<V>(new TvxIfThenElse<V, E, VC>(cond, val_if_true, val_if_false));   \
}


IF_THEN_ELSE(Tvf, double, std::shared_ptr<Tvf>, std::shared_ptr<Tvf>, TvfConst)
IF_THEN_ELSE(Tvf, double, std::shared_ptr<Tvf>, double, TvfConst)
IF_THEN_ELSE(Tvf, double, double, std::shared_ptr<Tvf>, TvfConst)
IF_THEN_ELSE(Tvf, double, double, double, TvfConst)

IF_THEN_ELSE(Tvb, bool, std::shared_ptr<Tvb>, std::shared_ptr<Tvb>, TvbConst)
IF_THEN_ELSE(Tvb, bool, std::shared_ptr<Tvb>, bool, TvbConst)
IF_THEN_ELSE(Tvb, bool, bool, std::shared_ptr<Tvb>, TvbConst)


#define DEF_ITE(V, AT, AF)                                                                  \
def(                                                                                        \
    "if_then_else",                                                                         \
    static_cast<                                                                            \
        std::shared_ptr<V> (*)(                                                             \
            std::shared_ptr<Tvb> cond,                                                      \
            AT val_if_true,                                                                 \
            AF val_if_false                                                                 \
        )                                                                                   \
    >(if_then_else),                                                                        \
    "Choose one of two values based on the value of a boolean. All time varying.",          \
    py::arg("condition"),                                                                   \
    py::arg("true_value") = 1.0,                                                            \
    py::arg("falsse_value") = 0.0                                                           \
)

#define TVX_DEF_OPS(__op__, __rop__, name, tv_operand_t, operand_t, operand_ct, ret_t)              \
def(# __op__, [](                                                                                   \
        const std::shared_ptr<tv_operand_t> lhs,                                                    \
        const std::shared_ptr<tv_operand_t> rhs                                                     \
    ) -> std::shared_ptr<ret_t> {                                                                   \
        return std::shared_ptr<ret_t>(new ret_t ## tv_operand_t ## name ## tv_operand_t(lhs, rhs)); \
    }, py::is_operator())                                                                           \
.def(# __op__, [](                                                                                  \
        const std::shared_ptr<tv_operand_t> lhs,                                                    \
        const operand_ct rhs                                                                        \
    ) -> std::shared_ptr<ret_t> {                                                                   \
        return std::shared_ptr<ret_t>(new ret_t ## tv_operand_t ## name ## operand_t(lhs, rhs));    \
    }, py::is_operator())                                                                           \
.def(# __rop__, [](                                                                                 \
        const std::shared_ptr<tv_operand_t> rhs,                                                    \
        const operand_ct lhs                                                                        \
    ) -> std::shared_ptr<ret_t> {                                                                   \
        return std::shared_ptr<ret_t>(new ret_t ## operand_t ## name ## tv_operand_t(lhs, rhs));    \
    }, py::is_operator())


#define TVX_DEF_UNOP(__op__, name, tv_operand_t, ret_t)                     \
def(# __op__, [](                                                           \
        const std::shared_ptr<tv_operand_t> x                               \
    ) -> std::shared_ptr<ret_t> {                                           \
        return std::shared_ptr<ret_t>(new ret_t ## name(x));                \
    }, py::is_operator())

#define TVF_DEF_STD_UNOP(name)                                          \
 def(#name, unary_op_func<std::name>,   #name " applied to a Tvf.")     \
.def(#name, unary_op_func_d<std::name>, #name " applied to a Tvf.")

PYBIND11_MODULE(ctvx, m) {
    m.doc() = "Time varying floating point numbers implemented in C++ and bound to Python.";

    py::class_<Tvf, std::shared_ptr<Tvf>>tvf(m, "Tvf");
    tvf.def("__call__", &Tvf::eval)
       .TVX_DEF_OPS(__add__, __radd__, Add, Tvf, Double, double, Tvf)
       .TVX_DEF_OPS(__sub__, __rsub__, Sub, Tvf, Double, double, Tvf)
       .TVX_DEF_OPS(__mul__, __rmul__, Mul, Tvf, Double, double, Tvf)
       .TVX_DEF_OPS(__truediv__, __rtruediv__, Div, Tvf, Double, double, Tvf)
       .TVX_DEF_OPS(__floordiv__, __rfloordiv__, FloorDiv, Tvf, Double, double, Tvf)

       .TVX_DEF_OPS(__eq__, __req__, Eq, Tvf, Double, double, Tvb)
       .TVX_DEF_OPS(__ne__, __rne__, Ne, Tvf, Double, double, Tvb)
       .TVX_DEF_OPS(__gt__, __rlt__, Gt, Tvf, Double, double, Tvb)
       .TVX_DEF_OPS(__lt__, __rgt__, Lt, Tvf, Double, double, Tvb)
       .TVX_DEF_OPS(__ge__, __rle__, Ge, Tvf, Double, double, Tvb)
       .TVX_DEF_OPS(__le__, __rge__, Le, Tvf, Double, double, Tvb)

       .TVX_DEF_UNOP(__neg__, Neg, Tvf, Tvf)
       .TVX_DEF_UNOP(__abs__, Abs, Tvf, Tvf)

       .def("__pow__",
           [](
               const std::shared_ptr<Tvf> base,
               const std::shared_ptr<Tvf> exp
           ) -> std::shared_ptr<Tvf> {
               return std::shared_ptr<Tvf>(new TvfPow(base, exp));
           },
           "Time varying pow().")
       .def("__pow__",
           [](
               const std::shared_ptr<Tvf> base,
               double exp
           ) -> std::shared_ptr<Tvf> {
               return std::shared_ptr<Tvf>(new TvfPow(base, std::shared_ptr<Tvf>(new TvfConst(exp))));
           },
          "Time varying pow().")
       ;

    py::class_<Tvb, std::shared_ptr<Tvb>>tvb(m, "Tvb");
    tvb.def("__call__", &Tvb::eval)
        .TVX_DEF_OPS(__and__, __rand__, And, Tvb, Bool, bool, Tvb)
        .TVX_DEF_OPS(__or__,  __ror__,  Or,  Tvb, Bool, bool, Tvb)
        .TVX_DEF_OPS(__xor__, __rxor__, Xor, Tvb, Bool, bool, Tvb)

        .TVX_DEF_UNOP(__not__, Not, Tvb, Tvb)
        ;

    py::class_<TvfConst, std::shared_ptr<TvfConst>>(m, "TvfConst", tvf)
        .def(py::init<double>());

    py::class_<TvfRamp, std::shared_ptr<TvfRamp>>(m, "TvfRamp", tvf)
        .def(py::init<std::shared_ptr<Tvf>, std::shared_ptr<Tvf>, std::shared_ptr<Tvf>, double, double>());

    m.TVF_DEF_STD_UNOP(sqrt)

     .TVF_DEF_STD_UNOP(sin)
     .TVF_DEF_STD_UNOP(cos)
     .TVF_DEF_STD_UNOP(tan)

     .TVF_DEF_STD_UNOP(asin)
     .TVF_DEF_STD_UNOP(acos)
     .TVF_DEF_STD_UNOP(atan)

     .TVF_DEF_STD_UNOP(sinh)
     .TVF_DEF_STD_UNOP(cosh)
     .TVF_DEF_STD_UNOP(tanh)

     .TVF_DEF_STD_UNOP(asinh)
     .TVF_DEF_STD_UNOP(acosh)
     .TVF_DEF_STD_UNOP(atanh)
     ;

    m.def(
        "time",
        []() -> std::shared_ptr<Tvf> { return std::shared_ptr<Tvf>(new TvfTime()); },
        "A TVF that evaluates to the current time."
    );

    m.def(
        "ramp",
        static_cast<std::shared_ptr<Tvf>(*)(
            std::shared_ptr<Tvf> frm,
            std::shared_ptr<Tvf> to,
            double start,
            double duration
        )>(ramp),
        "A TVF that ramps up as a function of time.",
        py::arg("f0"), py::arg("f1"),
        py::arg("x0") = 0.0, py::arg("width") = 1.0
    ).def(
        "ramp",
        static_cast<std::shared_ptr<Tvf>(*)(
            double frm,
            std::shared_ptr<Tvf> to,
            double start,
            double duration
        )>(ramp),
        "A TVF that ramps up as a function of time.",
        py::arg("f0") = 0.0, py::arg("f1"),
        py::arg("x0") = 0.0, py::arg("width") = 1.0
    ).def(
        "ramp",
        static_cast<std::shared_ptr<Tvf>(*)(
            std::shared_ptr<Tvf> frm,
            double to,
            double start,
            double duration
        )>(ramp),
        "A TVF that ramps up as a function of time.",
        py::arg("f0"), py::arg("f1") = 1.0,
        py::arg("x0") = 0.0, py::arg("width") = 1.0
    ).def(
        "ramp",
        static_cast<std::shared_ptr<Tvf>(*)(
            double frm,
            double to,
            double start,
            double duration
        )>(ramp),
        "A TVF that ramps up as a function of time.",
        py::arg("f0") = 0.0, py::arg("f1") = 1.0,
        py::arg("x0") = 0.0, py::arg("width") = 1.0
    )
    ;

    m.def(
        "cut",
        static_cast<std::shared_ptr<Tvf>(*)(
            std::shared_ptr<Tvf> before,
            double t,
            std::shared_ptr<Tvf> after
        )>(cut),
        "A TVF whose value cuts at different times."
    ).def(
        "cut",
        static_cast<std::shared_ptr<Tvf>(*)(
            double before,
            double t,
            std::shared_ptr<Tvf> after
        )>(cut),
        "A TVF whose value cuts at different times."
    ).def(
        "cut",
        static_cast<std::shared_ptr<Tvf>(*)(
            std::shared_ptr<Tvf> before,
            double t,
            double after
        )>(cut),
        "A TVF whose value cuts at different times."
    ).def(
        "cut",
        static_cast<std::shared_ptr<Tvf>(*)(
            double before,
            double t,
            double after
        )>(cut),
        "A TVF whose value cuts at different times."
    );

    m.def(
        "once",
        once,
        "A TVF that wraps another and caches the value for a given time if called repeatedly.",
        py::arg("x")
    );

    m.def(
        "sample",
        sample,
        "Sample a TVF and interpolate linearly between the sample points."
    );

    m.def(
        "constant",
        [](double x) -> std::shared_ptr<Tvf> { return std::shared_ptr<Tvf>(new TvfConst(x)); },
        "A TVF that always returns a constant value.",
        py::arg("x")
    ).def(
        "constant",
        [](bool b) -> std::shared_ptr<Tvb> { return std::shared_ptr<Tvb>(new TvbConst(b)); },
        "A TVF that always returns a constant value.",
        py::arg("b")
    );

    m.def(
        "min",
        [](py::args args) -> std::shared_ptr<Tvf> { return std::shared_ptr<Tvf>(new TvfMin(args)); },
        "A Tvf that returns the minimum of the values of Tvfs passed to it as args."
    );

    m.def(
        "max",
        [](py::args args) -> std::shared_ptr<Tvf> { return std::shared_ptr<Tvf>(new TvfMax(args)); },
        "A Tvf that returns the maximum of the values of Tvfs passed to it as args."
    );

    m.DEF_ITE(Tvf, std::shared_ptr<Tvf>, std::shared_ptr<Tvf>)
     .DEF_ITE(Tvf, std::shared_ptr<Tvf>, double)
     .DEF_ITE(Tvf, double, std::shared_ptr<Tvf>)
     .DEF_ITE(Tvf, double, double)

     .DEF_ITE(Tvb, std::shared_ptr<Tvb>, std::shared_ptr<Tvb>)
     .DEF_ITE(Tvb, std::shared_ptr<Tvb>, bool)
     .DEF_ITE(Tvb, bool, std::shared_ptr<Tvb>);
}
