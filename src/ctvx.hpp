#include <vector>
#include <algorithm>
#include <map>
#include <cmath>
#include <limits>
#include <iostream>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;


class Tvf {
public:
    virtual ~Tvf() {}
    virtual double eval(const double t) = 0;
};


class TvfTime: public Tvf {
public:
    double eval(const double t) {
        return t;
    }
};


class TvfConst: public Tvf {
private:
    double _x;
public:
    TvfConst(double x) : _x(x) {}
    double eval(const double t) {
        return _x;
    }
};


class TvfTvfOpTvf: public Tvf {
protected:
    std::shared_ptr<Tvf> _lhs;
    std::shared_ptr<Tvf> _rhs;
public:
   TvfTvfOpTvf(std::shared_ptr<Tvf> lhs, std::shared_ptr<Tvf> rhs): _lhs(lhs), _rhs(rhs) {}
};


class TvfTvfOpDouble: public Tvf {
protected:
    std::shared_ptr<Tvf> _lhs;
    double _rhs;
public:
   TvfTvfOpDouble(std::shared_ptr<Tvf> lhs, double rhs): _lhs(lhs), _rhs(rhs) {}
};


class TvfDoubleOpTvf: public Tvf {
protected:
    double _lhs;
    std::shared_ptr<Tvf> _rhs;
public:
   TvfDoubleOpTvf(double lhs, std::shared_ptr<Tvf> rhs): _lhs(lhs), _rhs(rhs) {}
};


#define TVX_OP_CLASSES(prefix, name, op, tv_operand_t, operand_t, operand_ct, eval_t)               \
class prefix ## tv_operand_t ## name ## tv_operand_t:                                               \
        public prefix ## tv_operand_t ## Op ## tv_operand_t {                                       \
    public:                                                                                         \
        prefix ## tv_operand_t ## name ## tv_operand_t(                                             \
            std::shared_ptr<tv_operand_t> lhs,                                                      \
            std::shared_ptr<tv_operand_t> rhs                                                       \
        ) : prefix ## tv_operand_t ## Op ## tv_operand_t(lhs, rhs) {}                               \
        eval_t eval(const double t) {                                                                             \
            return _lhs->eval(t) op _rhs->eval(t);                                                    \
        }                                                                                           \
};                                                                                                  \
                                                                                                    \
class prefix ## tv_operand_t ## name ## operand_t: public                                           \
        prefix ## tv_operand_t ## Op ## operand_t {                                                 \
    public:                                                                                         \
        prefix ## tv_operand_t ## name ## operand_t(                                                \
            std::shared_ptr<tv_operand_t> lhs,                                                      \
            operand_ct rhs                                                                          \
        ) : prefix ## tv_operand_t ## Op ## operand_t(lhs, rhs) {}                                  \
        eval_t eval(const double t) {                                                                             \
            return _lhs->eval(t) op _rhs;                                                            \
        }                                                                                           \
};                                                                                                  \
                                                                                                    \
class prefix ## operand_t ## name ## tv_operand_t: public                                           \
        prefix ## operand_t ## Op ## tv_operand_t {                                                 \
    public:                                                                                         \
        prefix ## operand_t ## name ## tv_operand_t(                                                \
            operand_ct lhs,                                                                         \
            std::shared_ptr<tv_operand_t> rhs                                                       \
        ) : prefix ## operand_t ## Op ## tv_operand_t(lhs, rhs) {}                                  \
        eval_t eval(const double t) {                                                                             \
            return _lhs op _rhs->eval(t);                                                            \
        }                                                                                           \
};

TVX_OP_CLASSES(Tvf, Add, +, Tvf, Double, double, double)
TVX_OP_CLASSES(Tvf, Sub, -, Tvf, Double, double, double)
TVX_OP_CLASSES(Tvf, Mul, *, Tvf, Double, double, double)
TVX_OP_CLASSES(Tvf, Div, /, Tvf, Double, double, double)


static double floor_div(double lhs, double rhs) {
    return static_cast<double>(static_cast<int>(lhs / rhs));
}


class TvfTvfFloorDivTvf: public TvfTvfOpTvf {
public:
    TvfTvfFloorDivTvf(
        std::shared_ptr<Tvf> lhs,
        std::shared_ptr<Tvf> rhs
    ) : TvfTvfOpTvf(lhs, rhs) {}
    double eval(const double t) {
        return floor_div(_lhs->eval(t), _rhs->eval(t));
    }
};

class TvfTvfFloorDivDouble: public TvfTvfOpDouble {
public:
    TvfTvfFloorDivDouble(std::shared_ptr<Tvf> lhs, double rhs) : TvfTvfOpDouble(lhs, rhs) {}
    double eval(const double t) {
        return floor_div(_lhs->eval(t), _rhs);
    }
};

class TvfDoubleFloorDivTvf: public TvfDoubleOpTvf {
public:
    TvfDoubleFloorDivTvf(double lhs, std::shared_ptr<Tvf> rhs) : TvfDoubleOpTvf(lhs, rhs) {}
    double eval(const double t) {
        return floor_div(_lhs, _rhs->eval(t));
    }
};


class TvfNeg: public Tvf {
protected:
    std::shared_ptr<Tvf> _x;
public:
   TvfNeg(std::shared_ptr<Tvf> x): _x(x) {}

   double eval(const double t) {
       return -(_x->eval(t));
   }
};


class TvfPow: public Tvf {
protected:
    std::shared_ptr<Tvf> _base;
    std::shared_ptr<Tvf> _exp;
public:
   TvfPow(std::shared_ptr<Tvf> base, std::shared_ptr<Tvf> exp): _base(base), _exp(exp) {}

   double eval(const double t) {
       return std::pow(_base->eval(t), _exp->eval(t));
   }
};


class TvfAbs: public Tvf {
protected:
    std::shared_ptr<Tvf> _x;
public:
   TvfAbs(std::shared_ptr<Tvf> x): _x(x) {}

   double eval(const double t) {
       double x = _x->eval(t);
       return std::abs(x);
   }
};


typedef double (*unary_op)(double);

template <unary_op F>
class TvfUnaryOp: public Tvf {
private:
    std::shared_ptr<Tvf> _x;
public:
   TvfUnaryOp(std::shared_ptr<Tvf> x): _x(x) {}

   double eval(const double t) {
       double x = _x->eval(t);
       return F(x);
   }
};

template <unary_op F>
std::shared_ptr<Tvf> unary_op_func(std::shared_ptr<Tvf> x) {
    return std::shared_ptr<Tvf>(new TvfUnaryOp<F>(x));
}

template <unary_op F>
std::shared_ptr<Tvf> unary_op_func_d(double x) {
    return std::shared_ptr<Tvf>(new TvfUnaryOp<F>(std::shared_ptr<Tvf>(new TvfConst(x))));
}


class TvfRamp: public Tvf {
private:
    std::shared_ptr<Tvf> _x;
    std::shared_ptr<Tvf> _f0;
    std::shared_ptr<Tvf> _f1;
    double _x0;
    double _w;

public:
    TvfRamp(
        std::shared_ptr<Tvf> x,
        std::shared_ptr<Tvf> f0,
        std::shared_ptr<Tvf> f1,
        double x0 = 0.0,
        double width = 1.0
    ) : _x(x), _f0(f0), _f1(f1), _x0(x0), _w(width)
    {}

    double eval(const double t) {
        double x = _x->eval(t) - _x0;
        if(x <= 0.0) {
            return _f0->eval(t);
        } else if(x >= _w) {
            return _f1->eval(t);
        } else {
            double m = (_w >= 0.0) ? (_f1->eval(t) - _f0->eval(t)) / _w : 0.0;

            return _f0->eval(t) + m * x;
        }
    }
};


class TvfCut: public Tvf {
private:
    std::vector<std::shared_ptr<Tvf>> _values;
    std::vector<double> _cut_times;
protected:
    friend std::shared_ptr<Tvf> cut(
       std::shared_ptr<Tvf> before,
       double t,
       std::shared_ptr<Tvf> after
   );

   std::vector<std::shared_ptr<Tvf>> values() {
       return _values;
   }

   std::vector<double> cut_times() {
       return _cut_times;
   }

public:
    TvfCut(std::vector<std::shared_ptr<Tvf>> &values, std::vector<double> &cut_times):
        _values(values), _cut_times(cut_times)
    {}

    double eval(const double t) {
        auto ii = std::upper_bound(_cut_times.begin(), _cut_times.end(), t) - _cut_times.begin();

        return _values[ii]->eval(t);
    }
};


class TvfMinMax: public Tvf {
protected:
    std::vector<std::shared_ptr<Tvf>> _args;
public:
    TvfMinMax(py::args args) :
        _args()
    {
        if(!args.size()) {
            throw std::invalid_argument("expected at least 1 argument, got 0");
        }

        for(py::handle arg: args) {
            try {
                std::shared_ptr<Tvf> tvf_arg = arg.cast<std::shared_ptr<Tvf>>();
                _args.push_back(std::shared_ptr<Tvf>(tvf_arg));
            } catch(py::cast_error e) {
                double double_arg = arg.cast<double>();
                _args.push_back(std::shared_ptr<Tvf>(new TvfConst(double_arg)));
            }
        }
    }
};


class TvfMin: public TvfMinMax {
public:
    TvfMin(py::args args) :
        TvfMinMax(args)
    {}

    double eval(const double t) {
        double min = std::numeric_limits<double>::max();

        for(std::shared_ptr<Tvf> arg : _args) {
            double v = arg->eval(t);
            min = std::min(min, v);
        }

        return min;
    }
};


class TvfMax: public TvfMinMax {
public:
    TvfMax(py::args args) :
        TvfMinMax(args)
    {}

    double eval(const double t) {
        double max = std::numeric_limits<double>::min();

        for(std::shared_ptr<Tvf> arg : _args) {
            double v = arg->eval(t);
            max = std::max(max, v);
        }

        return max;
    }
};


class TvfSample: public Tvf {
private:
    std::shared_ptr<Tvf> _x;
    std::vector<double> _sample_times;
    std::map<double, double> _samples;
public:
    TvfSample(std::shared_ptr<Tvf> x, std::vector<double> &sample_times) :
        _x(x),
        _sample_times(sample_times),
        _samples()
    {
        std::sort(_sample_times.begin(), _sample_times.end());
    }

    double _sample(double t) {
        double v;
        auto vi = _samples.find(t);
        if(vi == _samples.end()) {
            v = _x->eval(t);
            _samples.insert(std::pair<double, double>(t, v));
        } else {
            v = vi->second;
        }
        return v;
    }

    double eval(const double t) {
        unsigned long ii = std::lower_bound(_sample_times.begin(), _sample_times.end(), t) - _sample_times.begin();
        if(ii == 0) {
            double v = _sample(_sample_times.front());
            return v;
        } else if(ii == _sample_times.size()) {
            double v = _sample(_sample_times.back());
            return v;
        }

        double t0 = _sample_times[ii - 1];
        double t1 = _sample_times[ii];

        double x0 = _sample(t0);
        double x1 = _sample(t1);

        double s = (t - t0) / (t1 - t0);
        double v = (1 - s) * x0 + s * x1;

        return v;
    }
};


class TvfOnce: public Tvf {
private:
    std::shared_ptr<Tvf> _x;
    double _last_time = -1.0;
    double _cached_value = 0.0;
public:
    TvfOnce(std::shared_ptr<Tvf> x): _x(x) {}

    double eval(const double t) {
        if(t != _last_time) {
            _cached_value = _x->eval(t);
            _last_time = t;
        }
        return _cached_value;
    }
};


class Tvb {
public:
    virtual ~Tvb() {}
    virtual bool eval(const double t) = 0;
};


class TvbTvfOpTvf: public Tvb {
protected:
    std::shared_ptr<Tvf> _lhs;
    std::shared_ptr<Tvf> _rhs;
public:
   TvbTvfOpTvf(std::shared_ptr<Tvf> lhs, std::shared_ptr<Tvf> rhs) {
       _lhs = lhs;
       _rhs = rhs;
   }
};


class TvbTvfOpDouble: public Tvb {
protected:
    std::shared_ptr<Tvf> _lhs;
    double _rhs;
public:
   TvbTvfOpDouble(std::shared_ptr<Tvf> lhs, double rhs) {
       _lhs = lhs;
       _rhs = rhs;
   }
};


class TvbDoubleOpTvf: public Tvb {
protected:
    double _lhs;
    std::shared_ptr<Tvf> _rhs;
public:
   TvbDoubleOpTvf(double lhs, std::shared_ptr<Tvf> rhs) {
       _lhs = lhs;
       _rhs = rhs;
   }
};


class TvbTvbOpTvb: public Tvb {
protected:
    std::shared_ptr<Tvb> _lhs;
    std::shared_ptr<Tvb> _rhs;
public:
   TvbTvbOpTvb(std::shared_ptr<Tvb> lhs, std::shared_ptr<Tvb> rhs) {
       _lhs = lhs;
       _rhs = rhs;
   }
};


class TvbTvbOpBool: public Tvb {
protected:
    std::shared_ptr<Tvb> _lhs;
    bool _rhs;
public:
   TvbTvbOpBool(std::shared_ptr<Tvb> lhs, bool rhs) {
       _lhs = lhs;
       _rhs = rhs;
   }
};


class TvbBoolOpTvb: public Tvb {
protected:
    bool _lhs;
    std::shared_ptr<Tvb> _rhs;
public:
   TvbBoolOpTvb(bool lhs, std::shared_ptr<Tvb> rhs) {
       _lhs = lhs;
       _rhs = rhs;
   }
};


TVX_OP_CLASSES(Tvb, Eq, ==, Tvf, Double, double, bool)
TVX_OP_CLASSES(Tvb, Ne, !=, Tvf, Double, double, bool)
TVX_OP_CLASSES(Tvb, Lt,  <, Tvf, Double, double, bool)
TVX_OP_CLASSES(Tvb, Gt,  >, Tvf, Double, double, bool)
TVX_OP_CLASSES(Tvb, Le, <=, Tvf, Double, double, bool)
TVX_OP_CLASSES(Tvb, Ge, >=, Tvf, Double, double, bool)

TVX_OP_CLASSES(Tvb, And, &, Tvb, Bool, bool, bool)
TVX_OP_CLASSES(Tvb, Or,  |, Tvb, Bool, bool, bool)
TVX_OP_CLASSES(Tvb, Xor, ^, Tvb, Bool, bool, bool)


class TvbConst: public Tvb {
private:
    bool _x;
public:
    TvbConst(bool x) : _x(x) {}
    bool eval(const double t) {
        return _x;
    }
};


class TvbNot: public Tvb {
protected:
    std::shared_ptr<Tvb> _x;
public:
   TvbNot(std::shared_ptr<Tvb> x): _x(x) {}

   bool eval(const double t) {
       return !(_x->eval(t));
   }
};


template<class V, class E, class VC>
class TvxIfThenElse: public V {
private:
    std::shared_ptr<Tvb> _cond;
    std::shared_ptr<V> _val_if_true;
    std::shared_ptr<V> _val_if_false;
public:
    TvxIfThenElse(
        std::shared_ptr<Tvb> cond,
        std::shared_ptr<V> val_if_true,
        std::shared_ptr<V> val_if_false
    ) :
        _cond(cond), _val_if_true(val_if_true), _val_if_false(val_if_false)
    {}

    TvxIfThenElse(
        std::shared_ptr<Tvb> cond,
        E val_if_true,
        std::shared_ptr<V> val_if_false
    ) :
        _cond(cond),
        _val_if_true(std::shared_ptr<V>(new VC(val_if_true))),
        _val_if_false(val_if_false)
    {}

    TvxIfThenElse(
        std::shared_ptr<Tvb> cond,
        std::shared_ptr<V> val_if_true,
        E val_if_false
    ) :
        _cond(cond),
        _val_if_true(val_if_true),
        _val_if_false(std::shared_ptr<V>(new VC(val_if_false)))
    {}

    TvxIfThenElse(
        std::shared_ptr<Tvb> cond,
        E val_if_true,
        E val_if_false
    ) :
        _cond(cond),
        _val_if_true(std::shared_ptr<V>(new VC(val_if_true))),
        _val_if_false(std::shared_ptr<V>(new VC(val_if_false)))
    {}

    E eval(const double t) {
        if(_cond->eval(t)) {
            return _val_if_true->eval(t);
        } else {
            return _val_if_false->eval(t);
        }
    }
};
