Gewel and TVX
=============

Gewel is a package for scripting and rendering
animated videos. It is built on top of tvx, which is
a package for working with values that change over
time.

Most users will spend the majority of their time
working with gewel, and relatively little digging
into the lower levels of tvx. But a basic understanding
of tvx is helpful to get the most out of gewel.

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

   gewel </gewel/index>
   tvx </tvx/index>

At the moment, gewel and tvx source code
and documentation live in the same repository and and built and distributed
together. It is possible that as some future time they
may be separated, particularly if tvx proves to be
useful for some other application.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
