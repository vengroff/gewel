from gewel.color import GREEN, ORANGE
from gewel.draw import MarkerX, MarkerPlus, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

# Script d0
d0 = MarkerX(x=32, y=100, color=ORANGE, line_width=3)
d0.move_to(608, 200, duration=2.0)
d0.move_to(608, 100, duration=1.0)
d0.move_to(32, 100, duration=1.5)

# Link the motion of d1 to that of d0.
# It will follow d0 but 100 pixels below.
d1 = MarkerPlus(x=0, y=0, color=GREEN, line_width=3)
d1.x = d0.x
d1.y = d0.y + 100

# Note that we can produce the same results by
# setting x and y directly in the constructor.
d1 = MarkerPlus(x=d0.x, y=d0.y + 100, color=GREEN, line_width=3)

# Assemble the scene.
scene = Scene([background, d0, d1])

# Rendering phase:

recorder = GifRecorder('motion_track_x.gif')
recorder.record(scene)
