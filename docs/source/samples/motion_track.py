from gewel.color import GREEN, ORANGE
from gewel.draw import MarkerX, MarkerPlus, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

# Script d0
d0 = MarkerX(x=32, y=100, color=ORANGE, line_width=3)
d0.move_to(32, 200, duration=1.0)
d0.move_to(608, 100, duration=2.0)
d0.move_to(32, 100, duration=2.0)

# Link the motion of d1 to that of d0.
# It will follow d0 but 100 pixels below.
d1 = MarkerPlus(x=32, y=200, color=GREEN, line_width=3)
d1.track(d0, 0.0, 100.0)

# Assemble the scene.
scene = Scene([background, d0, d1])

# Rendering phase:

recorder = GifRecorder('motion_track.gif')
recorder.record(scene)
