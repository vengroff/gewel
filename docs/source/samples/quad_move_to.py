from gewel.color import ORANGE
from gewel.draw import MarkerUpArrow, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

drawable = MarkerUpArrow(x=32, y=440, height=64, color=ORANGE, line_width=3)

scaffold = drawable.quadratic_move_to(320, 40, 608, 440, duration=2.0, scaffold=True)

scene = Scene([background, drawable, scaffold])

# Rendering phase:

recorder = GifRecorder('quad_move_to.gif')
recorder.record(scene)
