import numpy as np

from gewel.draw import PngDrawable, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

drawable = PngDrawable('donut.png', x=320, y=240)

drawable.rotate_to(2 * np.pi, duration=2.0)

scene = Scene([background, drawable])

# Rendering phase:

recorder = GifRecorder('png_drawable.gif')
recorder.record(scene)
