from gewel.color import YELLOW, BLUE, GRAY
from gewel.draw import MarkerDot, Background, Scene, TextBox, TextVerticalPosition
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

# Sun
sun = MarkerDot(x=320, y=240, width=100, color=YELLOW)
sun.wait(4.0)
sun_label = TextBox(
    "Sun", x=370, y=190, width=100, height=16,
    vertical_position=TextVerticalPosition.BOTTOM,
    z=1.0
)
sun_label.track(sun, 50, -50)

# Planet
planet = MarkerDot(x=120, y=240, width=30, color=BLUE)
planet.orbit(sun, 200, 200, orbit_duration=4.0)
planet_label = TextBox(
    "Planet", x=140, y=220, width=100, height=16,
    vertical_position=TextVerticalPosition.BOTTOM,
    z=1.0, font_size=10.0
)
planet_label.track(planet, 20, -20)

# Moon
moon = MarkerDot(x=70, y=240, width=10, color=GRAY)
moon.orbit(planet, 50, 50, orbit_duration=1.0)
moon_label = TextBox(
    "Moon", x=85, y=225, width=100, height=16,
    vertical_position=TextVerticalPosition.BOTTOM,
    z=0.9, font_size=9.0
)
moon_label.track(moon, 15, -15)

# Assemble the scene.
scene = Scene([background, sun, sun_label, planet, planet_label, moon, moon_label])

# Rendering phase:

recorder = GifRecorder('solar_system.gif')
recorder.record(scene)
