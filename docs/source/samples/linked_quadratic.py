from gewel.color import BLUE, ORANGE, RED
from gewel.draw import Background, QuadraticCurveDrawable, Scene, MarkerPlus, MarkerX
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

point0 = MarkerPlus(40, 40, line_width=2, color=RED)
point1 = MarkerX(40, 440, line_width=2, color=ORANGE)
point2 = MarkerPlus(600, 440, line_width=2, color=BLUE)

# Make the control point move from corner to corner.

point1.move_to(600, 40, duration=1.5)
point1.move_to(40, 440, duration=1.5)

# A quadratic curve based on the points above, some
# of which will be in motion.
quad = QuadraticCurveDrawable(
    point0.x, point0.y,
    point1.x, point1.y,
    point2.x, point2.y,
)

scene = Scene([background, point0, point1, point2, quad])

# Rendering phase:

recorder = GifRecorder('linked_quadratic.gif')
recorder.record(scene)
