from gewel.color import ORANGE
from gewel.draw import MarkerUpArrow, Background, Scene, TimeClock
from gewel.record import GifRecorder

# Scripting phase:

background = Background()
clock = TimeClock(x=20, y=30, font_size=20, z=10.0)

drawable = MarkerUpArrow(x=32, y=240, height=64, color=ORANGE, line_width=3)

# Rotate while moving. We do this by not updating the
# drawable's next-action time during the move. As a
# result, the next action, which is the rotation, starts
# at the same time the move started. Since we did not
# add update_time=False to the rotation, the next-action time
# will be set to when the rotation ends, two seconds
# into the scene.
drawable.move_to(608, 240, duration=2.0, update_time=False)
drawable.rotate_to_degrees(360, duration=2.0)

# The next-action time is now two seconds into the scene.
# Let's wait two seconds doing nothing. That will leave the
# next-action time set to four seconds into the scene.
drawable.wait(duration=2.0)

# Now let's move back, again without updating the next-action
# time.
drawable.move_to(32, 240, duration=2.0, update_time=False)

# But this time we will wait for one second while we are moving,
# so the next-action time will be one second after the move
# started, which will be five seconds into the scene.
drawable.wait(duration=1.0)

# And them spin back during the final second of motion.
# This should begin at five seconds into the scene, when
# we have moved halfway back to the starting position. It
# should end six seconds into the scene when we are back
# at the initial position.
drawable.rotate_to_degrees(0, duration=1.0)

# Now wait for another two seconds. This takes us to the
# end of the scene at the eight second mark.
drawable.wait(duration=2.0)

scene = Scene([background, drawable, clock])

# Rendering phase:

recorder = GifRecorder('update_time.gif')
recorder.record(scene)
