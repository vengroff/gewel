from gewel.color import ORANGE
from gewel.draw import MarkerDot, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

drawable = MarkerDot(x=320, y=240, width=240, color=ORANGE, alpha=0.0)

drawable.fade_to(1.0, duration=2.0)
drawable.fade_to(0.0, duration=2.0)

scene = Scene([background, drawable])

# Rendering phase:

recorder = GifRecorder('fade_to.gif')
recorder.record(scene)
