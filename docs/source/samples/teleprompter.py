from gewel.color import ORANGE, YELLOW, Color
from gewel.draw import MarkerDot, Background, Scene, Teleprompter, sync
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

# Create our teleprompter.
teleprompter = Teleprompter(
    30, 400, 580, 70,
    font_size=24,
    color=YELLOW,
    fill_color=Color(0.25, 0.25, 0.25, 0.75)
)

# Start with an invisible drawable.
drawable = MarkerDot(x=320, y=240, width=64, color=ORANGE, alpha=0.0)

# Now we will alternately add text to the teleprompter
# and start actions we are describing in the animation.

teleprompter.add_script(
    """The orange dot is a mysterious creature. 
    
    Sometimes you don't even realize it is there, but
    it just slowly fades in.
    """
)

drawable.fade_to(1.0, duration=8.0)

# Whichever one is slower should wait for the other
# before the scene continues.
sync((drawable, teleprompter))

teleprompter.add_script(
    """But as soon as you see it...
    It quietly moves away.
    """
)

drawable.move_to(800, 600, duration=6.0)

scene = Scene([background, drawable, teleprompter])

# Rendering phase:

recorder = GifRecorder('teleprompter.gif')
recorder.record(scene)
