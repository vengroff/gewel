from gewel.color import BLUE, BLACK, ORANGE, PURPLE, RED
from gewel.draw import Background, Scene, MarkerX, MarkerO, \
    MarkerPlus, MarkerDot, MarkerUpArrow
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

mp = MarkerPlus(80, 120, line_width=5, width=100, color=BLUE)
mo = MarkerO(200, 360, line_width=5, width=100, color=ORANGE)
mx = MarkerX(320, 120, line_width=5, width=100, color=BLACK)
md = MarkerDot(440, 360, line_width=5, width=100, color=RED)
ma = MarkerUpArrow(560, 120, line_width=5, width=50, height=100, color=PURPLE)

mp.rotate_to_degrees(360, duration=3.0)
ma.rotate_to_degrees(-360, duration=3.0)

mo.move_to(200, 120, duration=1.5)
md.move_to(440, 120, duration=1.5)

mo.move_to(200, 360, duration=1.5)
md.move_to(440, 360, duration=1.5)

scene = Scene([background, mx, mp, ma, mo, md])

# Rendering phase:

recorder = GifRecorder('markers.gif')
recorder.record(scene)
