from gewel.color import ORANGE
from gewel.draw import MarkerUpArrow, Background, Scene
from gewel.record import GifRecorder

# Scripting phase:

background = Background()

drawable = MarkerUpArrow(x=320, y=240, height=64, color=ORANGE, line_width=3)

drawable.rotate_to_degrees(360, duration=2.0)

scene = Scene([background, drawable])

# Rendering phase:

recorder = GifRecorder('rotate_to_degrees.gif')
recorder.record(scene)
