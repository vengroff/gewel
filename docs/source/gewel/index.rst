.. _gewel_index:

Gewel
==========

Gewel is a package for scripting and rendering
animated videos.

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

   Getting Started <getting-started>
   The Drawing Lifecycle <drawing-lifecycle>
   Code Samples <code-samples>
   Gewel API reference <_autosummary/gewel>
