ctvx
=====

A C++ Implementation of Time-Varying X.
Ctvx has the same public API as :py:mod:`pytvx`, but
runs faster than the pure Python implementation.

.. note::
    For an introduction to TVX that is applicable
    whether you are using the Python or C++ implementation,
    please see :ref:`tvx_intro`.

For API reference and usage, please refer to
the documentation for :py:mod:`pytvx`.

