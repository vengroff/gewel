.. _tvx_index:

TVX (Time-Varying X)
====================

Tvx is a package that implements time-varying quantities. For
example, :py:class:`tvx.Tvf` is a time-varying floating point class.
Similarly, :py:class:`tvx.Tvb` is a time-varying boolean class.

.. toctree::
   :maxdepth: 2
   :titlesonly:
   :caption: Contents:

   Introduction <intro>
   pytvx - A pure Python implementation. <pytvx/index>
   ctvx - A C++ implementation. <ctvx>
