import unittest

import cairocffi as cairo

import gewel
import gewel.color
import gewel.draw
import gewel.record
import tvx
import tvx.utils
from tests.gewel.testutils import FileComparisonTest


class TestBoxContains(unittest.TestCase):
    def test_box_contains(self):
        box0 = gewel.draw.Box(
            tvx.constant(100), tvx.constant(1100),
            tvx.constant(100), 100,
            gewel.color.BLACK
        )

        self.assertTrue((box0.contains(150, 1150))(0))
        self.assertFalse((box0.contains(95, 1150))(0))
        self.assertFalse((box0.contains(205, 1150))(0))
        self.assertFalse((box0.contains(150, 1095))(0))
        self.assertFalse((box0.contains(150, 1205))(0))


class TestMarkers(FileComparisonTest):

    def testMarkers(self):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 640, 480)
        ctx = cairo.Context(surface)

        mx = gewel.draw.MarkerX(100, 200)
        mp = gewel.draw.MarkerPlus(100, 250)
        mo = gewel.draw.MarkerO(100, 300)

        mx.draw(ctx, 0)
        mp.draw(ctx, 0)
        mo.draw(ctx, 0)

        mx = gewel.draw.MarkerX(300, 100, line_width=5, width=100, color=gewel.color.Color(0.5, 0.0, 0.0))
        mp = gewel.draw.MarkerPlus(300, 250, line_width=5, width=100, color=gewel.color.Color(0.0, 0.5, 0.0))
        mo = gewel.draw.MarkerO(300, 400, line_width=5, width=100, color=gewel.color.Color(0.0, 0.0, 0.5))

        mx.draw(ctx, 0)
        mp.draw(ctx, 0)
        mo.draw(ctx, 0)

        filename = 'test_markers.png'
        path = self.artifact_path(filename)

        surface.write_to_png(path)

        ref_path = self.reference_path(filename)
        self.assertFilesEqual(path, ref_path)


class TestToyText(FileComparisonTest):

    def test_toy_text(self):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 640, 480)
        ctx = cairo.Context(surface)

        text0 = gewel.draw.ToyTextDrawable(100, 100, "Hello, World.")
        text0.draw(ctx, 0)

        big_text = gewel.draw.ToyTextDrawable(100, 400, "Big Text", font_size=120, color=gewel.color.MAROON)
        big_text.draw(ctx, 0)

        text_to_rotate = gewel.draw.ToyTextDrawable(0, 0, "Rotate me.", font_size=24, color=gewel.color.NAVY)
        rotated_text = gewel.draw.RotatedDrawable(text_to_rotate, 60 * gewel.draw._radians_per_degree)
        text1 = gewel.draw.TranslatedDrawable(rotated_text, 500, 100)
        text1.draw(ctx, 0)

        text_to_transform = gewel.draw.ToyTextDrawable(
            0, 0, "Stretch", font_size=30, color=gewel.color.DARK_GREEN
        )
        text2 = gewel.draw.TransformedDrawable(
            text_to_transform,
            3.0, 0.0, -1.0, 1.0, 100, 200
        )
        text2.draw(ctx, 0)

        filename = 'test_toy_text.png'
        path = self.artifact_path(filename)
        surface.write_to_png(path)

        ref_path = self.reference_path(filename)

        self.assertFilesEqual(path, ref_path)


class TestTextBox(FileComparisonTest):

    def test_text_box(self):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 640, 480)
        ctx = cairo.Context(surface)

        x, y, w, h = 100, 100, 100, 100

        b0 = gewel.draw.Box(x, y, w, h, color=gewel.color.LIGHT_GRAY)
        b0.draw(ctx, 0)

        tb0 = gewel.draw.TextBox("Hello, World. Nice to see you today!", x, y, w, h)
        tb0.draw(ctx, 0)

        x, y, w, h = 300, 100, 100, 100

        b1 = gewel.draw.Box(x, y, w, h, color=gewel.color.LIGHT_GRAY)
        b1.draw(ctx, 0)

        tb1 = gewel.draw.TextBox(
            """Hello, World.
            Another line.
            
            Me too.
            """,
            x, y, w, h,
            font_size=10
        )
        tb1.draw(ctx, 0)

        x, y, w, h = 100, 250, 200, 100

        b2 = gewel.draw.Box(x, y, w, h, color=gewel.color.LIGHT_GRAY)
        b2.draw(ctx, 0)

        # noinspection SpellCheckingInspection
        tb2 = gewel.draw.TextBox(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
            "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco " +
            "laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in " +
            "voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat " +
            "non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            x, y, w, h)
        tb2.draw(ctx, 0)

        x, y, w, h = 400, 250, 200, 100

        b3 = gewel.draw.Box(x, y, w, h, color=gewel.color.LIGHT_GRAY)
        b3.draw(ctx, 0)

        tb3 = gewel.draw.TextBox(
            "Center me!\n" +
            "And if I wrap around then just go ahead and keep on centering.\n" +
            "Center yourself!",
            x, y, w, h,
            justification=gewel.draw.TextJustification.CENTER, color=gewel.color.BLUE)
        tb3.draw(ctx, 0)

        x, y, w, h = 300, 400, 200, 50

        b4 = gewel.draw.Box(x, y, w, h, color=gewel.color.LIGHT_GRAY)
        b4.draw(ctx, 0)

        tb4 = gewel.draw.TextBox(
            "I_am_really_too_long_to_fit_in_this_narrow_little box.",
            x, y, w, h, color=gewel.color.RED, font_size=14)
        tb4.draw(ctx, 0)

        x, y, w, h = 500, 100, 100, 100

        tb5 = gewel.draw.TextBox(
            "Like a\nRAINBOW",
            x, y, w, h,
            font_size=16,
            justification=gewel.draw.TextJustification.CENTER,
            vertical_position=gewel.draw.TextVerticalPosition.MIDDLE,
            color=gewel.color.BLUE, fill_color=gewel.color.PURPLE, line_color=gewel.color.RED,
            line_width=5,
        )
        tb5.draw(ctx, 0)

        filename = 'test_text_box.png'
        path = self.artifact_path(filename)
        surface.write_to_png(path)

        ref_path = self.reference_path(filename)

        self.assertFilesEqual(path, ref_path)


class TestColorMap(unittest.TestCase):

    def assertSameColor(self, c0: gewel.color.BaseColor, c1: gewel.color.BaseColor):
        self.assertEqual(c0.tuple(0), c1.tuple(0))

    def test_color_map(self):
        cm = gewel.color.ColorMap.from_colors(
            [gewel.color.RED, gewel.color.GREEN, gewel.color.BLUE]
        )

        # At the fixed positions where colors are defined.
        cm.position = 0.0
        self.assertSameColor(gewel.color.RED, cm)
        cm.position = 0.5
        self.assertSameColor(gewel.color.GREEN, cm)
        cm.position = 1.0
        self.assertSameColor(gewel.color.BLUE, cm)

        # Interpolate.
        cm.position = 0.25
        self.assertSameColor(gewel.color.Color(0.5, 0.5, 0.0), cm)
        cm.position = 0.75
        self.assertSameColor(gewel.color.Color(0.0, 0.5, 0.5), cm)


class TestScaleToFit(FileComparisonTest):
    def test_scale_wide_to_narrow(self):
        for name, (from_width, from_height), (to_width, to_height) in [
            ["720p_vga", gewel.record.RESOLUTION_720P, gewel.record.RESOLUTION_VGA],
            ["vga_720p", gewel.record.RESOLUTION_VGA, gewel.record.RESOLUTION_720P],
            ["vga_vga", gewel.record.RESOLUTION_VGA, gewel.record.RESOLUTION_VGA],
            ["8K_1080p", gewel.record.RESOLUTION_8K, gewel.record.RESOLUTION_1080P],
        ]:
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, to_width, to_height)
            ctx = cairo.Context(surface)

            box = gewel.draw.Box(0, 0, from_width, from_height, color=gewel.color.TRANSPARENT,
                                 line_width=0.0, fill_color=gewel.color.GREEN)
            scaled_box = gewel.record.scale_to_fit(box, from_width, from_height, to_width, to_height)

            bg = gewel.draw.Background(gewel.color.BLACK)

            gewel.draw.Scene([bg, scaled_box]).draw(ctx, 0)

            filename = 'test_scale_{:s}.png'.format(name)
            path = self.artifact_path(filename)
            surface.write_to_png(path)

            ref_path = self.reference_path(filename)

            self.assertFilesEqual(path, ref_path)


if __name__ == '__main__':
    unittest.main()


class TestPng(FileComparisonTest):

    def test_png(self):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 640, 480)
        ctx = cairo.Context(surface)

        d = gewel.draw.PngDrawable(self.reference_path('donut.png'), centered=False)

        d.draw(ctx, 0)

        filename = 'test_donut.png'
        path = self.artifact_path(filename)
        surface.write_to_png(path)

        ref_path = self.reference_path(filename)

        self.assertFilesEqual(path, ref_path)

    def test_shear_png(self):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 640, 480)
        ctx = cairo.Context(surface)

        bg = gewel.draw.Background(gewel.color.Color(0.5, 0.75, 0.5))
        d = gewel.draw.PngDrawable(self.reference_path('donut.png'), centered=False)

        td = d.transformed(0.5, 0.0, 0.25, 0.5, 0.0, 0.0)

        bg.draw(ctx, 0)
        td.draw(ctx, 0)

        filename = 'test_shear_donut.png'
        path = self.artifact_path(filename)
        surface.write_to_png(path)

        ref_path = self.reference_path(filename)

        self.assertFilesEqual(path, ref_path)
