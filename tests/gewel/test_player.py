import unittest
import platform

import pytest

import gewel.draw

player = pytest.importorskip(
    "gewel.player",
    reason="The player cannot be instantiated if tkinter or Tk is not available."
)


@unittest.skipIf(platform.system() == 'Linux', "This test does not run on headless linux build machines.")
class TestInstantiate(unittest.TestCase):
    """
    Just make sure we can import and instantiate a
    minimal instance of the class.
    """
    def test_instantiate(self):
        marker = gewel.draw.MarkerX(100, 100)
        marker.wait(1)
        scene = gewel.draw.Scene([marker])
        _ = player.Player(scene)
