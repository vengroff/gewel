import numpy as np

import gewel
import gewel.contrib.tvdraw
import gewel.draw
import gewel.record
from gewel.color import Color, RED, BLUE, NAVY, DARK_GRAY, LIGHT_GRAY, PURPLE, WHITE, ORANGE
from tests.gewel.testutils import SceneComparisonTest
from tvx.utils import sine_wave


class TestAnimatedBox(SceneComparisonTest):

    def test_animated_box(self):
        bg = gewel.draw.Background()

        b0 = gewel.draw.Box(
            100, 100,
            100, 300,
            color=RED,
            fill_color=BLUE,
            line_width=10.0,
        )

        b0.ramp_attr_to('width', 400, duration=3.0)

        scene = gewel.draw.Scene([bg, b0])

        self.assertSceneEquals('test_animated_box.mp4', scene)


class TestSpinning(SceneComparisonTest):
    def test_spinning(self):
        """
        Test spinning an image around with the rotate_to method.
        """
        d = gewel.draw.PngDrawable(
            self.reference_path('donut.png'),
            x=320, y=240,
        )

        d.rotate_to(2 * 2 * np.pi, duration=3.0)

        self.assertSceneEquals('test_spinning_donut.mp4', d)


class TestCurves(SceneComparisonTest):

    def test_quadratic(self):
        bg = gewel.draw.Background()

        x0, y0 = 100, 100
        x1, y1 = 400, 300
        x2, y2 = 500, 100

        points = gewel.draw.Scene(
            [gewel.draw.MarkerX(x0, y0), gewel.draw.MarkerX(x1, y1), gewel.draw.MarkerX(x2, y2)],
            z=0.5
        )

        curve = gewel.draw.QuadraticCurveDrawable(
            x0, y0, x1, y1, x2, y2,
            color=Color(0.5, 0.5, 1.0),
            line_width=2,
            z=0.25
        )

        marker_o = gewel.draw.MarkerO(x=x0, y=y0, color=RED, z=1.0)
        marker_o.quadratic_move_to(x1, y1, x2, y2, duration=2.0)

        scene = gewel.draw.Scene([bg, points, curve, marker_o])

        self.assertEqual(2.0, scene.time)

        self.assertSceneEquals('test_quadratic_path.mp4', scene)

    def test_bezier_cubic(self):
        bg = gewel.draw.Background(z=-1.0)

        x0, y0 = 100, 100
        x1, y1 = 150, 300
        x2, y2 = 350, 300
        x3, y3 = 500, 100

        points = gewel.draw.Scene(
            [gewel.draw.MarkerX(x0, y0), gewel.draw.MarkerX(x1, y1),
             gewel.draw.MarkerX(x2, y2), gewel.draw.MarkerX(x3, y3)],
            z=0.5
        )

        curve = gewel.draw.BezierDrawable(
            x0, y0, x1, y1, x2, y2, x3, y3,
            color=Color(0.5, 0.5, 1.0),
            line_width=2,
            z=0.25
        )

        marker_o = gewel.draw.MarkerO(x=x0, y=y0, color=RED, z=1.0)
        marker_o.bezier_move_to(x1, y1, x2, y2, x3, y3, duration=2.0)

        scene = gewel.draw.Scene([bg, points, curve, marker_o])

        self.assertSceneEquals('test_cubic_path.mp4', scene)


class TestDynamicMoveTo(SceneComparisonTest):
    def test_toss(self):
        player1 = gewel.draw.MarkerO(100, 100, width=30, line_width=4, color=RED)
        player2 = gewel.draw.MarkerO(540, 100, width=30, line_width=4, color=RED)

        player1.move_to(100, 380, duration=2.0)
        player2.move_to(540, 380, duration=2.0)

        ball = gewel.draw.MarkerDot(player1.x, player1.y, width=20, color=PURPLE, z=1.0)
        ball.wait(0.5)
        ball.move_to(player2.x, player2.y, duration=1.0)

        ball.wait_for(player1)

        player1.move_to(100, 100, duration=2.0)
        player2.move_to(540, 100, duration=2.0)

        ball.track(player2)
        ball.wait(0.5)
        ball.move_to(player1.x, player1.y, duration=1.0)

        scene = gewel.draw.Scene([player1, player2, ball])

        self.assertSceneEquals('test_play_ball.mp4', scene)


class TestDynamicAlpha(SceneComparisonTest):
    def test_pulse(self):
        m1 = gewel.draw.MarkerDot(x=320, y=240, width=64, color=RED,
                                  alpha=sine_wave(frequency=0.25))
        m2 = gewel.draw.MarkerDot(x=160, y=240, width=32, color=BLUE, alpha=1.0 - m1.alpha)
        m3 = gewel.draw.MarkerDot(x=480, y=240, width=32, color=BLUE, alpha=1.0 - m1.alpha)

        m1.wait(8.0)

        scene = gewel.draw.Scene([m1, m2, m3])

        self.assertSceneEquals('test_pulse.mp4', scene)

    def test_fade(self):
        b = gewel.draw.Background(color=WHITE)
        m = gewel.draw.MarkerDot(x=40, y=240, width=64, color=NAVY)
        m.move_to(600, 240, duration=4.0, update_time=False)
        m.fade_to(0.0, duration=2.0)
        m.fade_to(1.0, duration=2.0)

        scene = gewel.draw.Scene([b, m])

        self.assertSceneEquals('test_fade.mp4', scene)


class TestScaffolding(SceneComparisonTest):
    def test_scaffolding(self):
        marker = gewel.draw.MarkerPlus(x=32, y=240)

        scaffold_1 = marker.move_to(608, 240, duration=2.0, scaffold=True)
        scaffold_2 = marker.quadratic_move_to(320, 0, 32, 240, duration=2.0,
                                              update_time=False, scaffold=True)
        scaffold_3 = marker.rotate_to_degrees(360, duration=2.0, scaffold=True)
        scaffold_4 = marker.bezier_move_to(
            320, 480, 320, 0,
            608, 240, duration=3.0,
            update_time=False, scaffold=True
        )
        scaffold_5 = marker.rotate_to_degrees(0, duration=3.0, scaffold=True)

        scene = gewel.draw.Scene([
            scaffold_1, scaffold_2, scaffold_3, scaffold_4, scaffold_5
        ])

        self.assertSceneEquals('test_scaffolding.mp4', scene)


class TestPngAlpha(SceneComparisonTest):
    def test_fading_donut(self):
        d = gewel.draw.PngDrawable(
            self.reference_path('donut.png'),
            x=320, y=240,
        )

        spin_duration = 6
        d.rotate_to(2 * np.pi, duration=spin_duration, update_time=False)
        for _ in range(spin_duration):
            d.fade_to(0.0, duration=0.5)
            d.fade_to(1.0, duration=0.5)

        b = gewel.draw.Background()
        scene = gewel.draw.Scene([b, d])

        self.assertSceneEquals('test_fading_donut.mp4', scene)


class TestSineWave(SceneComparisonTest):
    def test_sin_wave(self):
        m = gewel.draw.MarkerX(
            x=320 + sine_wave(frequency=0.5, amplitude=260, phase=-np.pi / 2),
            y=240 + sine_wave(frequency=0.5, amplitude=180),
            width=64, line_width=4, color=RED
        )
        m.rotate_to(theta=4 * np.pi, duration=6)
        b = gewel.draw.Background()
        scene = gewel.draw.Scene([b, m])

        self.assertSceneEquals('test_sin_ellipse.mp4', scene)


class TestOrbit(SceneComparisonTest):
    def test_orbit(self):
        center = 320, 240

        sun = gewel.draw.MarkerX(
            x=center[0], y=center[1], width=20, color=ORANGE, line_width=6, z=1
        )
        planet = gewel.draw.MarkerDot(width=30, color=BLUE)
        planet.orbit(center, 200, 100, 4)
        moon1 = gewel.draw.MarkerDot(width=10, color=DARK_GRAY)
        moon1.orbit(planet, 50, 50, 2)
        moon2 = gewel.draw.MarkerDot(width=6, color=PURPLE)
        moon2.orbit(planet, 80, 40, 4, ccw=True)

        tracker_x = gewel.draw.MarkerPlus(x=planet.x, y=20, width=20, color=BLUE, line_width=3)
        tracker_y = gewel.draw.MarkerPlus(y=planet.y, x=20, width=20, color=BLUE, line_width=3)

        line_x = gewel.draw.PathDrawable(
            [planet.x, tracker_x.x],
            [planet.y, tracker_x.y],
            color=LIGHT_GRAY, z=-1
        )

        line_y = gewel.draw.PathDrawable(
            [planet.x, tracker_y.x],
            [planet.y, tracker_y.y],
            color=LIGHT_GRAY, z=-1
        )

        planet.wait(8)

        scene = gewel.draw.Scene([
            sun, planet, moon1, moon2, tracker_x, tracker_y, line_x, line_y
        ])

        self.assertSceneEquals('test_orbit.mp4', scene)


class TestTeleprompter(SceneComparisonTest):
    def test_teleprompter(self):
        bg = gewel.draw.Background(z=-1.0)

        teleprompter = gewel.draw.Teleprompter(
            50, 350, 540, 100,
            fill_color=LIGHT_GRAY, color=NAVY,
            line_color=DARK_GRAY, line_width=3.0,
            font_size=36
        )

        teleprompter.add_text("This is the opening line.", 0.0)
        teleprompter.add_text("This is another line.", 3.0)
        teleprompter.add_text(
            "Sometimes a line is long and it just keeps going on. " +
            "So you just have to keep talking and talking some more.",
            4.5
        )
        teleprompter.add_text("Right after the last line.")
        teleprompter.add_text("After a long pause...\n\n", 15.0)
        teleprompter.add_text('<fin>')

        scene = gewel.draw.Scene([bg, teleprompter])

        self.assertSceneEquals('test_teleprompter.mp4', scene, duration=20.0)


class TestSceneClip(SceneComparisonTest):
    def test_clip(self):
        """
        Like our earlier spinning test, but now we clip the
        scene.
        """
        d = gewel.draw.PngDrawable(
            self.reference_path('donut.png'),
            x=320, y=240,
        )

        d.rotate_to(2 * 2 * np.pi, duration=3.0)

        # Only a slice works for a drawable.
        with self.assertRaises(IndexError):
            _ = d[1]

        # Create a clip then a scene around it.
        c = d[1.0:2]
        self.assertEqual(1.0, c.time)
        self.assertSceneEquals('test_spinning_donut_clip.mp4', c)

        # Wrap in a scene and then clip.
        s = gewel.draw.Scene([d])
        c = s[1:2.0]
        self.assertEqual(1.0, c.time)
        self.assertSceneEquals('test_spinning_donut_clip.mp4', c)

        # A scene can take a single value index and return a frame.
        f = s[1.7]
        self.assertIsInstance(f, gewel.draw.Frame)


class TestColorMapBox(SceneComparisonTest):

    def test_color_map_box(self):
        bg = gewel.draw.Background(z=-1.0)

        cmb1 = gewel.draw.ColorMapBox(x=500, y=20, width=40, height=440,
                                      color_map=gewel.color.fire.r(), z=-1.0)

        cmb2 = gewel.draw.ColorMapBox(x=20, y=20, width=440, height=40,
                                      vertical=False,
                                      color_map=gewel.color.gwv(),
                                      border_color=DARK_GRAY, line_width=2)

        cmb3 = gewel.draw.ColorMapBox(x=20, y=120, width=440, height=40,
                                      vertical=False, steps=1,
                                      color_map=gewel.color.gwv(),
                                      border_color=DARK_GRAY, line_width=2)

        cmb4 = gewel.draw.ColorMapBox(x=20, y=220, width=440, height=40,
                                      vertical=False, steps=2,
                                      color_map=gewel.color.gwv(),
                                      border_color=DARK_GRAY, line_width=2)

        cmb5 = gewel.draw.ColorMapBox(x=20, y=320, width=440, height=40,
                                      vertical=False, steps=10,
                                      color_map=gewel.color.gwv(),
                                      border_color=DARK_GRAY, line_width=2)

        cmb1.move_to(20, 20, 2.0)

        for cmb in [cmb2, cmb3, cmb4, cmb5]:
            cmb.move_to(100, cmb.y, 2.0, update_time=False)

        scene = gewel.draw.Scene([bg, cmb1, cmb2, cmb3, cmb4, cmb5])

        self.assertSceneEquals('test_colormap_box.mp4', scene)


class TestSceneSequence(SceneComparisonTest):

    def test_scene_sequence(self):
        m0 = gewel.draw.MarkerX(x=120, y=240, width=128, color=ORANGE, line_width=3, alpha=0)
        m0.fade_to(1.0, duration=1)
        m0.wait(1.0)
        m0.fade_to(0.0, duration=2)
        s0 = gewel.draw.Scene([m0])

        m1 = gewel.draw.MarkerPlus(x=320, y=240, width=128, color=ORANGE, line_width=3, alpha=0)
        m1.fade_to(1.0, duration=1)
        m1.wait(1.0)
        m1.fade_to(0.0, duration=2)
        s1 = gewel.draw.Scene([m1])

        m2 = gewel.draw.MarkerX(x=520, y=240, width=128, color=ORANGE, line_width=3, alpha=0)
        m2.fade_to(1.0, duration=1)
        m2.wait(1.0)
        m2.fade_to(0.0, duration=2)
        s2 = gewel.draw.Scene([m2])

        ss = gewel.draw.SceneSequence([s0, s1])
        ss.append(s2)

        self.assertSceneEquals("test_scene_sequence.mp4", ss)
