import tempfile
import unittest

import gewel.draw
import gewel.record


class TestInstantiate(unittest.TestCase):

    """
    Just make sure we can import and instantiate
    minimal instances of the classes.
    """
    def test_instantiate(self):
        file = tempfile.NamedTemporaryFile(suffix=".mp4")
        _ = gewel.record.Mp4Recorder(file)
        file.close()

        file = tempfile.NamedTemporaryFile(suffix=".gif")
        _ = gewel.record.GifRecorder(file)
        file.close()

        with tempfile.TemporaryDirectory() as dir:
            _ = gewel.record.FrameFileRecorder(dir)
