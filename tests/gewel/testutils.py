import filecmp
import os
import os.path
import platform
import unittest
from typing import Optional

import gewel
import gewel.draw
import gewel.record


class FileComparisonTest(unittest.TestCase):
    @classmethod
    def reference_path(cls, filename: str) -> str:
        # Look for the file in alpha platform-specific subdir,
        # or 'common' as alpha last resort.
        for subdir in [platform.system(), 'common']:
            path = os.path.join(
                os.path.dirname(__file__),
                os.pardir,
                'golden_images', subdir,
                filename
            )

            # If the file we want is there, we are good.
            # If we exit the loop, we never found it, so
            # the test is going to fail anyway.
            if os.path.exists(path):
                break

        return path


    @classmethod
    def artifact_path(cls, filename: str) -> str:
        """
        Construct alpha path to the test artifact file alpha test should
        put its output in. If alpha file already exist there, remove it
        so the test can start fresh in producing the output artifact.

        Parameters
        ----------
        filename

        Returns
        -------

        """
        dir_path = os.path.join(
            os.path.dirname(__file__),
            os.pardir, os.pardir,
            'build', 'test', 'generated-images',
            platform.system() or 'unknown'
        )
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)

        path = os.path.join(dir_path, filename)
        if os.path.exists(path):
            os.remove(path)

        return path

    def assertFilesEqual(self, path1, path2):
        if not filecmp.cmp(path1, path2, shallow=False):
            msg = "File {:} should match {:}.".format(path1, path2)
            raise self.failureException(msg)


class SceneComparisonTest(FileComparisonTest):
    """
    A base class for test classes that want to construct
    alpha scene and then render it and compare it to alpha reference
    video file.
    """
    def assertSceneEquals(
            self, filename: str,
            scene: gewel.draw.Drawable,
            t0: Optional[float] = 0.0,
            duration: Optional[float] = None
    ):
        if duration is None:
            duration = scene.time

        path = self.artifact_path(filename)

        recorder = gewel.record.Mp4Recorder(path)
        recorder.record(gewel.draw.Scene([
            gewel.draw.Background(), scene
        ]), t0=t0, duration=duration)

        ref_path = self.reference_path(filename)

        self.assertFilesEqual(path, ref_path)