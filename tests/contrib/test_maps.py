import os.path

from shapely.geometry import Polygon, MultiPolygon

import gewel.contrib.maps as maps
import gewel.draw
from gewel.color import RED, LIGHT_GRAY, DARK_GRAY, BLACK, BLUE, category10
from tests.gewel.testutils import SceneComparisonTest


class TestPolygonDrawable(SceneComparisonTest):
    def test_polygon(self):
        coordinates = ((100., 50.), (100., 250.), (400., 250.), (400., 50.), (100., 50.))
        holes = [
            ((200., 125.), (200., 175.), (250., 175.)),
            ((300., 125.), (300., 175.), (250., 125.)),
        ]
        polygon = Polygon(
            coordinates,
            holes,
        )
        d = maps.ShapelyDrawable(polygon, color=RED, fill_color=LIGHT_GRAY, line_width=5.0)

        self.assertEqual(250.0, d.x)
        self.assertEqual(150.0, d.y)
        self.assertEqual(300.0, d.width)
        self.assertEqual(200.0, d.height)

        d.move_to(470, 360, duration=2)

        multi_coordinates = [
            (
                ((10, 10), (10, 60), (60, 60), (60, 10)),
                [
                    ((12, 12), (12, 18), (18, 18), (18, 12)),
                    ((52, 52), (52, 58), (58, 58), (58, 52)),
                ]
            ),
            (
                ((10, 110), (10, 160), (60, 160), (60, 110)),
                [
                    ((12, 112), (12, 118), (18, 118), (18, 112)),
                    ((52, 152), (52, 158), (58, 158), (58, 152)),
                ]
            ),
        ]

        multi_polygon = MultiPolygon(multi_coordinates)

        m = maps.ShapelyDrawable(
            multi_polygon, color=BLACK, fill_color=LIGHT_GRAY,
            centered=True
        )

        self.assertEqual(50.0, m.width)
        self.assertEqual(150.0, m.height)

        m.rotate_to_degrees(90, duration=2, update_time=False)
        m.move_to(100, 100, duration=1)

        x = gewel.draw.MarkerX(100, 100, color=BLUE, z=-1.0)

        scene = gewel.draw.Scene([
            d, m, x
        ])

        self.assertSceneEquals("test_map_poly.mp4", scene)


class TestStates(SceneComparisonTest):
    def setUp(self) -> None:
        path = os.path.join(
            os.path.dirname(__file__),
            'resources', 'shapefiles',
            'tri-state-area'
        )
        self.gdf = maps.read_shapefile(path)

    def test_states(self):
        state_geos = [{"geometry": g} for g in self.gdf.geometry]

        for ii in range(len(state_geos)):
            state_geos[ii]['fill_color'] = category10(ii)

        map_drawable = maps.MapDrawable(state_geos, x=20, y=20, width=600, height=440, color=LIGHT_GRAY)
        self.assertEqual(600, map_drawable.width)

        box = gewel.draw.Box(x=20, y=20, width=600, height=440, color=BLUE, z=-1, line_width=1)

        map_drawable.rotate_to_degrees(360, 2)

        scene = gewel.draw.Scene([box, map_drawable])

        self.assertSceneEquals("test_states.mp4", scene)

    def test_states2(self):
        state_geos = [{"geometry": g} for g in self.gdf.geometry]

        scene_drawables = []

        for x, y, width, height, kwargs in [
            (20, 20, 100, 100, {'color': RED, 'line_width': 0.5}),
            (100, 200, 500, 200, {'color': DARK_GRAY, 'fill_color': BLUE, 'line_width': 2.0})
        ]:

            map_drawable = maps.MapDrawable(
                state_geos, x=x, y=y, width=width, height=height, **kwargs
            )
            self.assertEqual(width, map_drawable.width)

            box = gewel.draw.Box(
                x=x - 5, y=y - 5,
                width=width + 10, height=height + 10,
                color=BLUE, z=-1,
                line_width=1
            )

            map_drawable.rotate_to_degrees(360, 2)

            scene_drawables.append(box)
            scene_drawables.append(map_drawable)

        m = gewel.draw.MarkerX(
            620 - scene_drawables[1].width / 2,
            scene_drawables[1].y,
            width=100, color=BLUE, z=-1
        )

        scene_drawables[1].move_to(
            m.x, m.y,
            duration=2
        )

        scene_drawables.append(m)

        scene = gewel.draw.Scene(scene_drawables)

        self.assertSceneEquals("test_states2.mp4", scene)
