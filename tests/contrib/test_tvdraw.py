import gewel
import tvx
from gewel.color import RED, BLUE
from tests.gewel.testutils import SceneComparisonTest


class TestTvDrawable(SceneComparisonTest):
    def test_tvd_animated_box(self):
        """
        This should duplicate :py:meth:`TestAnimatedBox.test_animated_box`
        but using a :py:class:`~tvx.Tvd` to hold the time-varying arguments
        and creating a new drawable for each frame.

        This is less efficient for simple cases like this but easier to manage
        for complex cases where there is a lot of computation and possibly
        third-party libraries involved in the construction of a drawable based
        on one or more time-varying values.
        """
        bg = gewel.draw.Background()

        @gewel.contrib.tvdraw.time_varying_drawable
        def _box(x: float, y: float, width: float, height: float):
            return gewel.draw.Box(
                x, y,
                width, height,
                color=RED,
                fill_color=BLUE,
                line_width=10.0,
            )

        tvd = tvx.Tvd(x=100, y=100, width=100, height=300)
        tvd.ramp_item_to('width', 400, duration=3.0)

        b0 = _box(tvd)

        scene = gewel.draw.Scene([bg, b0])

        self.assertSceneEquals('test_animated_box.mp4', scene)
