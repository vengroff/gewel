import itertools
import math
import unittest
from dataclasses import dataclass

import numpy as np

import tvx
import tvx.utils


@dataclass()
class ThingWithData:
    x: tvx.FloatOrTVF = 0.0
    y: tvx.FloatOrTVF = 0.0
    z: tvx.FloatOrTVF = 0.0


def _float(x: tvx.FloatOrTVF, t: float = 0.0):
    if isinstance(x, tvx.Tvf):
        return x(t)
    else:
        return x


def _bool(b: tvx.BoolOrTVB, t: float = 0.0):
    if isinstance(b, tvx.Tvb):
        return b(t)
    else:
        return b


class TestCut(unittest.TestCase):
    def test_cut(self):
        thing = ThingWithData(x=10, y=20, z=30)

        ramp_up_time = 2.0
        cut_at_time = 3.0
        ramp_down_time = 2.0

        # Before the cut.
        ramp_up = tvx.ramp(width=ramp_up_time)

        # After the cut.
        ramp_down = tvx.ramp(
            f0=300, f1=200,
            x0=cut_at_time,
            width=ramp_down_time
        )

        # Cut
        cut_lt_x = tvx.cut(100 + 200 * ramp_up, cut_at_time, ramp_down)

        # Assign it to alpha property of the thing.
        thing.x = cut_lt_x

        # Now run through time:
        for t in np.linspace(0.0, ramp_up_time):
            self.assertAlmostEqual(100 + 200 * t / ramp_up_time, _float(thing.x, t), places=10)

        for t in np.linspace(ramp_up_time, cut_at_time):
            self.assertAlmostEqual(300, _float(thing.x, t), places=10)

        for t in np.linspace(cut_at_time, cut_at_time + ramp_down_time):
            self.assertAlmostEqual(300 - 100 * (t - cut_at_time) / ramp_down_time, _float(thing.x, t), places=10)

        for t in np.linspace(cut_at_time + ramp_down_time, cut_at_time + ramp_down_time + 1):
            self.assertAlmostEqual(200, _float(thing.x, t), places=10)

    def test_ramp_cut(self):

        thing = ThingWithData()

        ramp_up_start_time = 1.0
        ramp_up_time = 2.0
        cut_at_time = 4.0
        ramp_down_time = 2.0

        end_time = cut_at_time + ramp_down_time + 1.0

        thing.x = tvx.ramp(100, 200, ramp_up_start_time, ramp_up_time)
        thing.x = tvx.cut(thing.x, cut_at_time, tvx.ramp(200, 0, cut_at_time, ramp_down_time))

        for t in np.linspace(0.0, ramp_up_start_time):
            self.assertAlmostEqual(100, _float(thing.x, t), places=10)

        for t in np.linspace(ramp_up_start_time, ramp_up_start_time + ramp_up_time):
            self.assertAlmostEqual(100 + 100 * (t - ramp_up_start_time) / ramp_up_time, _float(thing.x, t), places=10)

        for t in np.linspace(ramp_up_start_time + ramp_up_time, cut_at_time):
            self.assertAlmostEqual(200, _float(thing.x, t), places=10)

        for t in np.linspace(cut_at_time, cut_at_time + ramp_down_time):
            self.assertAlmostEqual(200 - 200 * (t - cut_at_time) / ramp_down_time, _float(thing.x, t), places=10)

        for t in np.linspace(cut_at_time + ramp_down_time, end_time):
            self.assertAlmostEqual(0, _float(thing.x, t), places=10)


class TestTimeVaryingX(unittest.TestCase):

    def test_global_time(self):
        time = tvx.time()

        self.assertEqual(0.0, time(0))
        self.assertEqual(1.0, (time + 1.0)(0))
        self.assertEqual(1.0, (1.0 + time)(0))
        self.assertEqual(-1.0, (time - 1.0)(0))
        self.assertEqual(1.0, (1.0 - time)(0))
        self.assertEqual(0.0, (2.0 * time)(0))
        self.assertEqual(2.0, (time + 1.0 + 1.0)(0))

        self.assertTrue((0.0 == time)(0))
        self.assertTrue((1.0 == time + 1.0)(0))
        self.assertTrue((1.0 == 1.0 + time)(0))
        self.assertTrue((-1.0 == time - 1.0)(0))
        self.assertTrue((1.0 == 1.0 - time)(0))
        self.assertTrue((0.0 == 2.0 * time)(0))
        self.assertTrue((2.0 == time + 1.0 + 1.0)(0))

    def test_tvx_ops(self):
        """
        Test ops on TVFs and make sure than they
        produce the same results as on floats.

        I.e. float(t1 op t2) == float(t1) op float(t2)
        """
        float_abc = [1.0, 2.0, 3.0]
        tvf_abc = [tvx.constant(x) for x in float_abc]

        for a in itertools.chain(tvf_abc, float_abc):
            self.assertEqual(_float(-a), -_float(a))

            self.assertEqual(_float(abs(a)), abs(_float(a)))
            self.assertEqual(_float(abs(-a)), abs(_float(-a)))

            self.assertAlmostEqual(_float(tvx.sqrt(a)), math.sqrt(_float(a)), places=10)

            self.assertAlmostEqual(_float(tvx.sin(a)), math.sin(_float(a)), places=10)
            self.assertAlmostEqual(_float(tvx.cos(a)), math.cos(_float(a)), places=10)
            self.assertAlmostEqual(_float(tvx.tan(a)), math.tan(_float(a)), places=10)

            self.assertAlmostEqual(_float(tvx.asin(1 / a)), math.asin(_float(1 / a)), places=10)
            self.assertAlmostEqual(_float(tvx.acos(1 / a)), math.acos(_float(1 / a)), places=10)
            self.assertAlmostEqual(_float(tvx.atan(1 / a)), math.atan(_float(1 / a)), places=10)

            self.assertAlmostEqual(_float(tvx.sinh(a)), math.sinh(_float(a)), places=10)
            self.assertAlmostEqual(_float(tvx.cosh(a)), math.cosh(_float(a)), places=10)
            self.assertAlmostEqual(_float(tvx.tanh(a)), math.tanh(_float(a)), places=10)

            self.assertAlmostEqual(_float(tvx.asinh(a)), math.asinh(_float(a)), places=10)
            self.assertAlmostEqual(_float(tvx.acosh(1 + a)), math.acosh(_float(1 + a)), places=10)
            self.assertAlmostEqual(_float(tvx.atanh(a / 5)), math.atanh(_float(a / 5)), places=10)

            for b in itertools.chain(tvf_abc, float_abc):
                self.assertEqual(_float(a + b), _float(a) + _float(b))
                self.assertEqual(_float(a - b), _float(a) - _float(b))
                self.assertEqual(_float(a * b), _float(a) * _float(b))
                self.assertEqual(_float(a / b), _float(a) / _float(b))

                self.assertEqual(_bool(a == b), _float(a) == _float(b))
                self.assertEqual(_bool(a != b), _float(a) != _float(b))
                self.assertEqual(_bool(a > b), _float(a) > _float(b))
                self.assertEqual(_bool(a < b), _float(a) < _float(b))
                self.assertEqual(_bool(a >= b), _float(a) >= _float(b))
                self.assertEqual(_bool(a <= b), _float(a) <= _float(b))

                for c in itertools.chain(tvf_abc, float_abc):
                    self.assertEqual(_bool((a < b) & (a < c)), _float(a) < _float(b) and _float(a) < _float(c))
                    self.assertEqual(_bool((a > b) & (a > c)), _float(a) > _float(b) and _float(a) > _float(c))
                    self.assertEqual(_bool((a <= b) & (a <= c)), _float(a) <= _float(b) and _float(a) <= _float(c))
                    self.assertEqual(_bool((a >= b) & (a >= c)), _float(a) >= _float(b) and _float(a) >= _float(c))

                    self.assertEqual(_bool((a < b) | (a < c)), _float(a) < _float(b) or _float(a) < _float(c))
                    self.assertEqual(_bool((a > b) | (a > c)), _float(a) > _float(b) or _float(a) > _float(c))
                    self.assertEqual(_bool((a <= b) | (a <= c)), _float(a) <= _float(b) or _float(a) <= _float(c))
                    self.assertEqual(_bool((a >= b) | (a >= c)), _float(a) >= _float(b) or _float(a) >= _float(c))

    def test_sample(self):
        t = tvx.time()
        t2 = t * t

        t_s = tvx.utils.lin_space_sample(t2, 0.0, 10.0, 11)

        # At the sample points, they match.
        for gt in np.linspace(0.0, 10.0, 11):
            self.assertEqual(t2(gt), t_s(gt))

        # In between sample points, linear interpolation
        # of the sample point quadratic values.
        for gt in np.linspace(0.0, 9.0, 10):
            for offset in np.linspace(0.1, 0.9, 9):
                gt2 = gt * gt
                gt12 = (gt + 1) * (gt + 1)
                gto2 = (gt + offset) * (gt + offset)
                lin = (1 - offset) * gt2 + offset * gt12

                self.assertAlmostEqual(lin, t_s(gt + offset), places=10)
                self.assertAlmostEqual(gto2, t2(gt + offset), places=10)

        # Off the ends, they have the end values.

        self.assertEqual(0.0, t_s(-1.0))
        self.assertEqual(1.0, t2(-1.0))

        self.assertEqual(100.0, t_s(11.0))
        self.assertEqual(121.0, t2(11.0))

    def test_if_then_else(self):
        ite_t = tvx.if_then_else(tvx.constant(True), 1.0, 0.0)
        ite_f = tvx.if_then_else(tvx.constant(False), 1.0, 0.0)

        self.assertEqual(1.0, ite_t(0))
        self.assertEqual(0.0, ite_f(0))


class TestSun(unittest.TestCase):

    def test_sum(self):
        n = 1000

        leaves = [tvx.constant(6.0) for _ in range(n)]

        tvf_sum = sum(leaves)

        tvf_add = leaves[0]
        for leaf in leaves[1:]:
            tvf_add = tvf_add + leaf

        s0 = tvf_sum(0)
        s1 = tvf_add(0)

        self.assertEqual(s0, s1)
        self.assertEqual(6 * n, s0)


class TestMinMax(unittest.TestCase):

    def setUp(self) -> None:
        self.ramp = tvx.ramp(x0=1)

    def test_min(self):
        m = tvx.min(0.5, self.ramp, 0.25, 0.75)

        for t in np.linspace(0.0, 1.0):
            self.assertEqual(0.0, m(t))

        for t in np.linspace(1.0, 1.25):
            self.assertAlmostEqual(t - 1.0, m(t), places=10)

        for t in np.linspace(1.25, 3.0):
            self.assertEqual(0.25, m(t))

    def test_min(self):
        m = tvx.max(0.5, self.ramp, 0.25, 0.75)

        for t in np.linspace(0.0, 1.75):
            self.assertEqual(0.75, m(t))

        for t in np.linspace(1.75, 2.0):
            self.assertAlmostEqual(t - 1.0, m(t), places=10)

        for t in np.linspace(2.0, 3.0):
            self.assertEqual(1.0, m(t))

    def test_empty(self):
        with self.assertRaises(ValueError):
            tvx.min()
        with self.assertRaises(ValueError):
            tvx.max()


class TestTvd(unittest.TestCase):
    def test_construct_no_args(self):
        tvd = tvx.Tvd()

        self.assertEqual(0, len(tvd))

        with self.assertRaises(KeyError):
            tvd.get_at('key', 0)

        with self.assertRaises(KeyError):
            tvd.cut_item_to('key', 1.0)

        with self.assertRaises(KeyError):
            tvd.ramp_item_to('key', 1.0, 1.0, False)

    def test_construct_keys(self):
        tvd = tvx.Tvd(x=2, y=10)

        self.assertEqual(2, tvd['x'](0))
        self.assertEqual(10, tvd['y'](0))

        self.assertEqual(2, tvd.get_at('x', 1.23))
        self.assertEqual(10, tvd.get_at('y', 2.46))

        with self.assertRaises(KeyError):
            _ = tvd['z']

        tvd['y'] = -10

        self.assertEqual(-10, tvd.get_at('y', 2.46))

    def test_set_item(self):
        tvd = tvx.Tvd()

        tvd['x'] = 2
        tvd['y'] = 10

        self.assertEqual(2, tvd['x'](17.0))
        self.assertEqual(10, tvd['y'](99.9))

        self.assertEqual(2, tvd.get_at('x', 1.23))
        self.assertEqual(10, tvd.get_at('y', 2.46))

        with self.assertRaises(KeyError):
            _ = tvd['z']

    def test_cut_item_to(self):
        tvd = tvx.Tvd(x=10)

        tvd.wait(2.0)

        tvd.cut_item_to('x', 20)

        self.assertEqual(10, tvd.get_at('x', 0.0))
        self.assertEqual(10, tvd.get_at('x', 1.9))

        self.assertEqual(20, tvd.get_at('x', 2.0))
        self.assertEqual(20, tvd.get_at('x', 3.0))

    def test_ramp_item_to(self):
        tvd = tvx.Tvd(x=10)

        tvd.wait(1.0)
        tvd.ramp_item_to('x', 20, 1.0)

        self.assertEqual(10, tvd.get_at('x', 0.9))
        self.assertEqual(10, tvd.get_at('x', 1.0))
        self.assertEqual(15, tvd.get_at('x', 1.5))
        self.assertEqual(20, tvd.get_at('x', 2.0))
        self.assertEqual(20, tvd.get_at('x', 2.1))

    def test_get_item(self):
        tvd = tvx.Tvd(x=2.0)
        tvd.wait(10.0)
        tvd.ramp_item_to('x', 3.0, 1.0)

        self.assertEqual(2.0, tvd['x'](10.0))
        self.assertEqual(2.5, tvd['x'](10.5))
        self.assertEqual(3.0, tvd['x'](11.0))

    def test_at(self):
        tvd = tvx.Tvd(x=0.0, y=0.0, z=0.0)
        tvd.ramp_item_to('x', 1.0, 1.0)
        tvd.ramp_item_to('y', 2.0, 1.0)
        tvd.ramp_item_to('z', 10.0, 1.0)

        self.assertEqual(dict(x=1.0, y=1.0, z=0.0), tvd(1.5))

    def test_nested_at(self):
        tvd = tvx.Tvd(x=0.0, y=0.0, n=tvx.Tvd(z=0.0))
        tvd.ramp_item_to('x', 1.0, 1.0)
        tvd.ramp_item_to('y', 2.0, 1.0)
        tvd['n'].wait_for(tvd)
        tvd['n'].ramp_item_to('z', 10.0, 1.0)

        self.assertEqual(dict(x=1.0, y=1.0, n=dict(z=0.0)), tvd(1.5))


if __name__ == '__main__':
    unittest.main()
