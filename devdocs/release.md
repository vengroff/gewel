How to Release a New Version
============================

1. Make sure everything you want to put in the release is 
on the `main` branch.
2. Create a release branch called `rel-X.Y.Z` for 
the new branch.
3. On the release branch, edit `_version.py` and change the 
value of `release` to `X.Y.Z` with no `rc` suffix. 
4. Push the branch to remote so that it builds.
5. Check the build at https://gitlab.com/vengroff/gewel/-/pipelines
   and make sure it passed. If not, fix whatever the issue is and go
   back the previous step.
6. Make a git tag `X.Y.Z` on the commit that just successfully built.
7. Go to https://readthedocs.org/projects/gewel/versions/ and
activate a version of the docs for the tag you just made.
   1. Wait for the docs to build and verify that they are available 
   at https://gewel.readthedocs.io/en/X.Y.Z/ 
8. Go to the *wheel* job of the successful build and under the job
artifacts section click the browse button. 
   1. There should be a single
   directory called `wheelhouse`. 
   2. Inside the `wheelhouse` directory there should be a single
   file `Gewel-X.Y.Z-cp39-cp39-manylinux_2_24_x86_64.whl`. 
   3. Click on the file, then click download.
9. Upload the manylinux wheel to pypi
   1. Upload the wheel file to test pypi instance with the command
   `twine upload --repository-url https://test.pypi.org/legacy/ ~/Downloads/Gewel-X.Y.Z-cp39-cp39-manylinux_2_24_x86_64.whl`
   2. If that worked, upload it to the production with the command
   ` twine upload ~/Downloads/Gewel-X.Y.Z-cp39-cp39-manylinux_2_24_x86_64.whl `
   3. Verify that the wheel is available at https://pypi.org/project/Gewel/.
10. Now, for the Darwin version
    1. check out the tag X.Y.Z on a Mac
    2. In the virtual env for the project in your IDE, build the wheel
    with the command `python setup.py bdist_wheel`.
    3. The wheel should be at 
    `dist/Gewel-X.Y.Z-cp39-cp39-macosx_10_14_x86_64.whl`
    relative to the project root.
    4. As for Linux, upload to the test pypi with 
    `twine upload --repository-url https://test.pypi.org/legacy/ dist/Gewel-X.Y.Z-cp39-cp39-macosx_10_14_x86_64.whl`
    5. Assuming that worked properly, upload to production with the command
    `twine upload dist/Gewel-X.Y.Z-cp39-cp39-macosx_10_14_x86_64.whl`
    6. The Darwin file should be available alongside the manylinux
    one at https://pypi.org/project/Gewel/X.Y.Z/#files
11. Create a pull request to merge the release branch `rel-X.Y.Z` 
back onto `main` and merge it.
12. On the main branch, edit `_version.py` and set the value of
`release` to `X.Y.(Z+1)rc`. E.g. if you just released `1.2.3`, set
it to `1.2.4rc`. Alternatively, if you know the next release will be
and X or Y change, set the new value accordingly, with the `rc`
suffix.
