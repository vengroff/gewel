"""
Global version number for any place in the project that needs it.
"""

# Full version including rc suffix if the is a release
# candidate. E.g. 'X.Y.Z' or 'X.Y.Zrc'.
release = '0.3.1rc'
