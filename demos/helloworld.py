from gewel.draw import PngDrawable, Background, Scene
from gewel.player import Player

background = Background()

bep = PngDrawable('bep.png', x=32, y=240)

scaffold_1 = bep.move_to(608, 240, duration=2.0, scaffold=True)
scaffold_2 = bep.quadratic_move_to(320, 0, 32, 240, duration=2.0,
                                   update_time=False, scaffold=True)
scaffold_3 = bep.rotate_to_degrees(360, duration=2.0, scaffold=True)

scene = Scene([background, bep, scaffold_1, scaffold_2, scaffold_3])

# recorder = Mp4Recorder('helloworld.mp4')
# recorder.record(scene)

# recorder = GifRecorder('helloworld.gif')
# recorder.record(scene)

player = Player(scene)

player.mainloop()
