from multiprocessing import Pool, Process, Queue

import numpy as np
import cairocffi as cairo

from time import sleep


IS_PICKLABLE = False


class MaybePicklable:
    def __init__(self, x: int):
        self._x = x

    # A strange thing to do, but works for this
    # demo by making it easy to toggle picklability.
    def __getstate__(self):
        if IS_PICKLABLE:
            return {'_x': self._x}
        else:
            raise Exception("Can't pickle me!")

    def __call__(self, y: int) -> int:
        sleep(1)
        return self._x + y


mp = MaybePicklable(10)


def call_a_local_mp(y: int) -> int:
    local_mp = MaybePicklable(100)
    return local_mp(y)


class ProcessWithCairo(Process):
    _scene = None

    @staticmethod
    def set_scene(scene):
        ProcessWithCairo._scene = scene

    def __init__(self, width: int, height: int):
        super().__init__(daemon=True)
        self._width = width
        self._height = height
        self._request_queue = Queue()
        self._response_queue = Queue()

    @property
    def request_queue(self):
        return self._request_queue

    @property
    def response_queue(self):
        return self._response_queue

    def run(self):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self._width, self._height)
        ctx = cairo.Context(surface)

        while True:
            y = self._request_queue.get()
            print(f"Got {y}")
            if y < 0:
                break

            ctx.move_to(0, 0)
            ctx.set_line_width(y)
            ctx.line_to(self._width, self._height)
            ctx.stroke()

            buf = surface.get_data()
            rgba_data = np.ndarray(shape=(self._height, self._width, 4), dtype=np.uint8, buffer=buf)
            r_data, g_data, b_data = rgba_data[:, :, 0], rgba_data[:, :, 1], rgba_data[:, :, 2]
            data = np.stack([b_data, g_data, r_data], axis=2)

            print(f"data is a {type(data)}")
            self._response_queue.put(data)


def main():
    # In the single main process we can use the
    # object in a normal way whether it is
    # picklable or not.
    print(f"Direct call: mp(7) -> {mp(7)}")
    print(f"Direct call: call_a_local_mp(7) -> {call_a_local_mp(7)}")

    # Now let's try to map it and execute with
    # a pool.
    pool = Pool(processes=4)

    # This will not try to pickle.
    results = pool.map(call_a_local_mp, range(10))
    print("Mapped results with call_a_local_mp:")
    for result in results:
        print(f"    {result}")

    pool.close()
    pool.join()

    # Try processes and queues.
    process = ProcessWithCairo(64, 48)
    ProcessWithCairo.set_scene(MaybePicklable(0))

    process.start()

    process.request_queue.put(1)
    process.request_queue.put(2)
    process.request_queue.put(-1)

    r1 = process.response_queue.get()
    r2 = process.response_queue.get()

    print(len(r1), len(r1[0]), len(r2), len(r2[0]))

    process.join()
    process.close()


if __name__ == '__main__':
    main()